import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'
import { Col } from 'react-bootstrap'
import Card from 'react-bootstrap/Card'
import { Formik } from 'formik'
import { Pacienteschema } from '../models/PacienteModel'

export const Formulario = () => {
  return (
    <Card >
    <Card.Header><h4>Consultar Paciente</h4></Card.Header>
        <Card.Body>
        <Formik
            validationSchema={Pacienteschema}
            onSubmit={console.log}
            initialValues={{
              per_nombre1: 'Mark',
              per_apellido1: 'Otto'
            }}
            >
            {({
              handleSubmit,
              handleChange,
              handleBlur,
              values,
              touched,
              isValid,
              errors
            }) => (

        <Form noValidate onSubmit={handleSubmit}>
        <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom01">
            <Form.Label>Tipo de documento</Form.Label>
            <Form.Control as="select" >
                <option>Cedula Cuidadana</option>
                <option>Pasaporte</option>
            </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom02">
            <Form.Label>Documento</Form.Label>
            <Form.Control
                required
                type="Number"
                placeholder="Documento"
                onChange= {handleChange}
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom03">
            <Form.Label>Primer Apellido</Form.Label>
            <Form.Control
                required
                type="text"
                placeholder="Primer Apellido"
                defaultValue=""
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom04">
            <Form.Label>Segundo Apellido</Form.Label>
            <Form.Control
                type="text"
                placeholder="Segundo Apellido"
                defaultValue=""
            />
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom05">
            <Form.Label>Primer Nombre</Form.Label>
            <Form.Control
                name="per_nombre1"
                type="text"
                placeholder="Primer Nombre"
                value={values.per_nombre1}
                onChange={handleChange}
                isValid={touched.per_nombre1 && !errors.per_nombre1}
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom06">
            <Form.Label>Segundo Nombre</Form.Label>
            <Form.Control
                type="text"
                placeholder="Segundo Nombre"
                defaultValue=""
            />
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom07">
            <Form.Label>Sexo</Form.Label>
            <Form.Control as="select">
                <option>Masculino</option>
                <option>Femenino</option>
            </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom08">
            <Form.Label>Fecha de Nacimiento</Form.Label>
            <Form.Control
                required
                type="date"
                placeholder=""
                defaultValue=""
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="2" controlId="validationCustom09">
            <Form.Label>Edad</Form.Label>
            <Form.Control
                required
                type="Number"
                placeholder="Edad"
                defaultValue=""
            />
            <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="2" controlId="validationCustom10">
            <Form.Label>Medida Edad</Form.Label>
            <Form.Control as="select">
                <option>Años</option>
                <option>Meses</option>
            </Form.Control>
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom11">
            <Form.Label>Direccion</Form.Label>
            <Form.Control type="text" placeholder="City"/>
            </Form.Group>
            <Form.Group as={Col} md="3" controlId="validationCustom12">
            <Form.Label>Zona</Form.Label>
            <Form.Control as="select">
                <option>Urbana</option>
            </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="3" controlId="validationCustom13">
            <Form.Label>Telefóno</Form.Label>
            <Form.Control
                type="Number"
                placeholder="Telefóno"
                defaultValue=""
            />
            </Form.Group>
            <Form.Group as={Col} md="2" controlId="validationCustom14">
            <Form.Label>Extensión</Form.Label>
            <Form.Control
                type="Number"
                placeholder="Extensión"
                defaultValue=""
            />
            </Form.Group>
        </Form.Row>
        <Form.Row>
            <Form.Group as={Col} md="4" controlId="validationCustom15">
            <Form.Label>Celular</Form.Label>
            <Form.Control
                type="Number"
                placeholder="Celular"
                defaultValue=""
            />
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom16">
            <Form.Label>Correo</Form.Label>
            <InputGroup hasValidation>
                <InputGroup.Prepend>
                <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                </InputGroup.Prepend>
                <Form.Control
                type="email"
                placeholder="Correo"
                aria-describedby="inputGroupPrepend"
                />
                <Form.Control.Feedback type="invalid">
                Por favor ingrese un correo
                </Form.Control.Feedback>
            </InputGroup>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom17">
            <Form.Label>País Nacimiento</Form.Label>
            <Form.Control as="select">
                <option>Colombia</option>
            </Form.Control>
            </Form.Group>
        </Form.Row>
        <Form.Row>
        <Form.Group as={Col} md="4" controlId="validationCustom18">
            <Form.Label>Departamento Nacimiento</Form.Label>
            <Form.Control as="select">
                <option>Antioquia</option>
            </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom19">
            <Form.Label>Cuidad Nacimiento</Form.Label>
            <Form.Control as="select">
                <option>Antioquia</option>
            </Form.Control>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom20">
            <Form.Label>País Residencia</Form.Label>
            <Form.Control as="select">
                <option>Colombia</option>
            </Form.Control>
            </Form.Group>
        </Form.Row>
        <Button type="submit">Enviar Formulario</Button>
        </Form>
            )}
          </Formik>
        </Card.Body>
    </Card>
  )
}
