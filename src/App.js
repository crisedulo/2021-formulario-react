import { Formulario } from './components/Formulario'
import 'bootstrap/dist/css/bootstrap.min.css'

function App () {
  return (
    <div className="container mt-5" >
      <Formulario/>
    </div>
  )
}

export default App
